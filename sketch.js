/*jshint esversion: 6 */

let textInput;
let goButton;
let tree;
let codesDiv;

let yHeight = 130;
let minLowestInternalWidth = 40;
function setup() {
    textInput = createInput("The Cat in The Hat");
    goButton = createButton("go");
    goButton.mousePressed(process);
    createCanvas(10,10);
    codesDiv = createDiv("");
 
}

/**
*processes the tree and draws it to the canvas. 
*Triggered by the goButton press event 
*/
function process(){
    //create a new tree and process the initial character frequencies. 
    //(first conversted to lower case)
    tree = new Tree();
    let text = textInput.value().toLowerCase();
    console.log(text);
    for(var i in text){
        tree.addSymbolInstance(text.charAt(i));
    }

    //print a table of initial frequencies to the console.
    console.table(tree.nodes);
    
    tree.compileTree();
    
    //kick of outputting the tree and codes.  
    codesDiv.html("");
    tree.retrieveCodes(codesDiv);
    let l = tree.getLayers();
    //resize the canvas to fit the tree.
    resizeCanvas(pow(2,l)*minLowestInternalWidth, yHeight*(l+1));
    background(200);
    //draw the tree to the canvas.
    tree.drawTree(width/2, 20, width-50, yHeight, 25);

}

function draw() {
    noLoop();
}


