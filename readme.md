# Huffman encoder and visualiser

Simple implementation of huffman coding for a user provided string. 

Developed as part of the Fundamentals of computer science module delivered in 2017 at Goldsmiths. 

Feel free to branch, adapt, explore and improve as you like. 

## File description
* **index.html:** basic html file with links to the javascript source
* **sketch.js:** creates the canvas and other on-screen elements for the user interaction creates a new tree object 
* **tree.js:** stores the nodes of the tree. kicking off the drawing and encoding process
* **node.js:** individual elements of the tree, including  internal and leaf nodes. Lots of recursion in here! :)

## Huffman algorithm

>The simplest construction algorithm uses a priority queue where the node with lowest probability is given highest priority:

>* Create a leaf node for each symbol and add it to the priority queue.
* While there is more than one node in the queue:
* Remove the two nodes of highest priority (lowest probability) from the queue
* Create a new internal node with these two nodes as children and with probability equal to the sum of the two nodes' probabilities.
* Add the new node to the queue.
* The remaining node is the root node and the tree is complete.

taken from [Huffman coding wikipedia](https://en.wikipedia.org/wiki/Huffman_coding)

