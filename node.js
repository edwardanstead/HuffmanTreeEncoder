/*jshint esversion: 6 */


function Node(s){
	this.symbol = s;	//the symbol represent by a leaf node
	this.value = 1;		//frequency symbol(s) appear in the text
	this.left = null;	//left child subtree
	this.right = null;	//right child subtree
	this.layer = 0;		//tree layer, used for drawing the tree
	this.code = "";		//huffman code for the Node if leaf

	/**
	* set the value for the node based on the values of children
	*/
	Node.prototype.setValue = function(){
		this.value = this.left.value + this.right.value;
	};

	/**
	* Increase the layer this node appears in the tree
	*subsequently increse the layer of child subtrees
	*/
	Node.prototype.increaseLayer = function(){
		this.layer++;
		if(this.left)this.left.increaseLayer();
		if(this.right)this.right.increaseLayer();
	};


	/**
	*returns the deepest layer number in the tree iterating to the 
	*furthest leaf node.
	*@return {Integer} layer
	*/
	Node.prototype.getDeepestLayer = function(){
		if(this.symbol){
			return this.layer;
		}

		let lLayer = this.left.getDeepestLayer();
		let rLayer = this.right.getDeepestLayer();
		
		if(lLayer > rLayer){
			return lLayer;
		}

		else{
			return rLayer;
		}
	};

	/**
	* sets the code of the node, if a leaf node, otherwise recurssivly searches 
	* down the tree to a leaf node adding a 0 to s if searching left or 1 if 
	* searching right
	* @param {String} s
	*/
	Node.prototype.setCode = function(s){
		if(!s){
			s = "";
		}
		if(this.symbol != null){
			this.code = s;
			console.log(this.symbol + ": " + s);
			return;
		}

		if(this.left){
			this.left.setCode(s+"0");
		}

		if(this.right){
			this.right.setCode(s+"1");
		}
	};

	/**
	*Gets the code for a leaf node and packages it in a <p> tag for display
	*in the codeDiv otherwise searches left and right for a leaf node
	* @param {p5.Div} codeDiv
	*/
	Node.prototype.getCode = function(codeDiv){
		if(this.code){
			let codeP = createP("");
			//for a space between words add the word 'space' to the div
			//to avoid ambiguity 
			if(this.symbol == " "){
				codeP.html("space: " + this.code);

			}
			else{
				codeP.html(this.symbol + ": " + this.code);
			}
			codeDiv.child(codeP);
		}

		else{
			if(this.left) this.left.getCode(codeDiv);
			if(this.right) this.right.getCode(codeDiv);
		}
	};

	/**
	*drawing code for the node. for a internal node draws an ellipse containing the value fo the node as
	*text and two lines connecting to left and right subtrees, for a leaf shows the symbol
	*@param {Number} x - the x coordinate for the centre of the ellipse showing frequency or the symbol 
	*@param {Number} y - the y coordinate for the centre of the ellipse showing frequency or the symbol 
	*@param {Number} width - the width of the node
	*@param {Number} height - the height of the node
	*@param {Number} ellipseRad - the radius of the frequecny ellipse
	*/
	Node.prototype.drawNode = function(x, y, w, h, ellipseRad){
		stroke(0);
		fill(255);
		strokeWeight(3);
		textAlign(CENTER, CENTER);
		textSize(ellipseRad);

		//an internal node
		if(!this.symbol){
			//left and right lines to the next subtree
			line(x,
				y+ellipseRad,
				x-w*0.25, 
				y+h);

			line(x,
				y+ellipseRad,
				x+w*0.25, 
				y+h);
			ellipse(x,y+ellipseRad, ellipseRad*2);
			fill(0);
			strokeWeight(1);
			text(this.value, x, y+ellipseRad);
			//place text along each line to either 0 or 1 to show building of code
			textAlign(BOTTOM);
			textSize(ellipseRad/2);
			text("0", x-w*0.20, y+h*0.5);
			text("1", x+w*0.20, y+h*0.5);


			//calling the drawing function for left and righ subtrees
			//the new nodes start at the end of the subtree lines drawn above
			if(this.left){
				this.left.drawNode(x-w*0.25, y+h, w/2, h, ellipseRad);
			}

			if(this.right){
				this.right.drawNode(x+w*0.25, y+h, w/2, h, ellipseRad);
			}
		}
		//otherwise draw a leaf node
		else{

			fill(0);
			strokeWeight(1);
			if(this.symbol == " "){
				text("_", x, y+ellipseRad);
			}
			else{
				text(this.symbol, x, y+ellipseRad);
			}
		}

	};
}