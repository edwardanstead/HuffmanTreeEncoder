/*jshint esversion: 6 */

function Tree(){
	this.nodes = [];

	/**
	*check if the tree contains a node for the current symbol
	*@param {String} s
	*@return {Node | Boolean}
	*/
	Tree.prototype.checkForNode = function(symbol){
		for (let i in this.nodes){
			if (this.nodes[i].symbol == symbol){
				return this.nodes[i];
			}
		}

		return false;
	};

	/**
	*adds 1 to the value (frequency) for an existing node
	*if there is no node for the symbol a new node is added to the tree
	*@param {String} symbol 
	*/
	Tree.prototype.addSymbolInstance = function(symbol){
		let s = this.checkForNode(symbol);
		if(s){
			s.value++;
		}
		else{
			let node = new Node(symbol);
			this.nodes.push(node);
		}
	};

	/**
	*compiles the tree, while the tree length is greater than 1
	*creates a new node with the smallest existing two nodes as left and right children
	*/
	Tree.prototype.compileTree = function(){

		/*
		*finds the index of the tree element with the smallest value.
		*@return {Integer} si
		*/
		Tree.prototype.smallestIndex = function(){
			let si = null;
			for (let i in this.nodes){
				if(si == null || this.nodes[i].value < this.nodes[si].value){
					si = i;
				}
			}

			return si;
		};

		while(this.nodes.length > 1){
			
			let internalNode = new Node();
			
			//left subtree 
			let si = this.smallestIndex();
			internalNode.left = this.nodes[si];
			this.nodes.splice(si, 1);

			//right subtree
			let ssi = this.smallestIndex();
			internalNode.right = this.nodes[ssi];
			this.nodes.splice(ssi, 1);
						
			//calculate new node value and add to the tree
			//internalNode.value = internalNode.right.value + internalNode.left.value;
			internalNode.setValue();
			internalNode.increaseLayer();
			this.nodes.push(internalNode);

		}
	};

	/**
	*Calls the set and get codes methods for the root of the tree structure
	*@param {p5.Div} codesDiv
	*/
	Tree.prototype.retrieveCodes = function(codesDiv){
		this.nodes[0].setCode();
		
		this.nodes[0].getCode(codesDiv);

	};

	/**
	* calls the root nodes drawing function, see Node.drawNode for param desciptions 
	*@param {Number} x
	*@param {Number} y
	*@param {Number} width
	*@param {Number} height
	*@param {Number} ellipseRad
	*/

	Tree.prototype.drawTree = function(x,y, width, height, ellipseRad){
		//console.log(layers);
		this.nodes[0].drawNode(x,y,width, height, ellipseRad);
	}

	/**
	* calls the getDeepestLayer function for the root node
	* a helper function for the drawing code, the width of each layer
	*is established by how many layers the tree contains.
	*/
	Tree.prototype.getLayers = function(){
		return this.nodes[0].getDeepestLayer();
	}
}







